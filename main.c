
/* 
  Alexandra Gaspar, Evan Poncelet
  October 25, 2013

  Description: The main program that runs the Train Management System. It deals
               with all the initialization necessary, controlling the queue of 
               tasks, and keeping track of the time base

  Citations for code developped by others

  LED initialization
  - Author: Texas Instruments Incorporated
  - Date: 2007-2011
  - Title of program/source code: blinky.c
  - Type: source code

  PWM/beeping initialization 
  - Author: Texas Instruments Incorporated
  - Date: 2005-2011
  - Title of program/source code: pwmgen.c
  - Type: source code

    Timer initialization
  - Author: Texas Instruments Incorporated
  - Date: 2007-2011
  - Title of program/source code: timer.c
  - Type: source code

   uart initialization 
  - Author: Texas Instruments Incorporated
  - Date: 2005-2011
  - Title of program/source code: uart_echo.c
  - Type: source code

*/

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "C:/StellarisWare/boards/ek-lm3s8962/drivers/rit128x96x4.h"
#include "taskInfo.h"
#include "structs.h"
#include <stdio.h>
#include "inc/lm3s8962.h"
//includes for interrupts
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
//includes for hardware timer
#include "driverlib/timer.h"
//includes for serial communication 
#include "driverlib/uart.h"


//Global variables
unsigned short arrivalDirection,departureDirection,trainSize, traversalTime, trainArriving, 
arrivingWhistle,departingWhistle, mode, statusSelection, scroll, select, annunciation;
Bool checkTrain, trainPresent, gridlock, nLock, eLock, wLock, sLock;
unsigned int counter;

//additional Globals
unsigned int arrivalTime;
unsigned short fiveSecondCount, fourSecondCount, threeSecondCount, twoSecondCount, oneSecondCount;
int aBeepStartCount;
Bool aBeepLongMode;
Bool aBeepSilenceMode;
int aBeepLONG_BLAST;
int aBeepSHORT_BLAST;
int aBeepSILENCE;
int aBeepTOTAL_LONG_BLASTS;
int aBeepTOTAL_SHORT_BLASTS;
int aBeepNumLongBlasts;
int aBeepNumShortBlasts;

int dBeepStartCount;
Bool dBeepLongMode;
Bool dBeepSilenceMode;
int dBeepLONG_BLAST;
int dBeepSHORT_BLAST;
int dBeepSILENCE;
int dBeepTOTAL_LONG_BLASTS;
int dBeepTOTAL_SHORT_BLASTS;
int dBeepNumLongBlasts;
int dBeepNumShortBlasts;
  
unsigned int arrivalTime;
Bool annuncArrival;
Bool annuncDeparture;

//Task Data
ArrivingTrainStruct arrivingTrainData;
DepartingTrainStruct departingTrainData;
TrainComStruct trainComData;
DisplayStruct oledDisplayData;
SwitchControlStruct switchControlData;
LocalKeypadStruct localKeypadData;
SerialComStruct serialComData;

  /*char a[1]="a";
  char b[1]="b";
  char c[1]="c";
  char d[1]="d";
  char e[1]="e";
  char f[1]="f";
  char g[1]="g";*/

//TCB
TCB arrivingTrain;
TCB departingTrain;
TCB trainCom;
TCB oledDisplay;
TCB switchControl;
TCB localKeypad;
TCB serialCom;



// The error routine that is called if the driver library encounters an error.
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, unsigned long ulLine)
{
}
#endif
    
    
// Purpose: set the initial state of all the variables
void initializeVariables() 
{
  arrivalDirection = departureDirection = trainSize = 0;
  traversalTime = trainArriving = arrivingWhistle = 0;
  departingWhistle = mode = statusSelection = 0;
  scroll = select = annunciation = 0;
  checkTrain = trainPresent = gridlock = FALSE;
  nLock = eLock = wLock = sLock = FALSE;
  counter = 0;
  
  fiveSecondCount = 50;
  fourSecondCount = 40;
  threeSecondCount = 30;
  twoSecondCount  = 20;
  oneSecondCount  = 10;
  
  arrivalTime = 0;

  aBeepStartCount = 0;
  aBeepLongMode = 0;
  aBeepSilenceMode = 0;
  aBeepLONG_BLAST = 0;
  aBeepSHORT_BLAST = 0;
  aBeepSILENCE = 0;
  aBeepTOTAL_LONG_BLASTS = 0;
  aBeepTOTAL_SHORT_BLASTS = 0;
  aBeepNumLongBlasts = 0;
  aBeepNumShortBlasts = 0;
  
  annuncArrival = FALSE;
  annuncDeparture = FALSE;
  
  
}

// Purpose: initialize the data structs for the tasks
void initializeTaskData()
{

  //arriving train data initialization
  arrivingTrainData.trainArriving = &trainArriving;
  arrivingTrainData.checkTrain = &checkTrain;
  arrivingTrainData.arrivalDirection = &arrivalDirection;
    
  //departing train data initialization
  departingTrainData.trainPresent = &trainPresent;
  departingTrainData.departureDirection = &departureDirection;
  
  //train communication data initialization
  trainComData.trainPresent = &trainPresent;
  trainComData.checkTrain = &checkTrain;
  trainComData.trainSize = &trainSize;
  trainComData.trainArriving = &trainArriving;
  trainComData.arrivalDirection = &arrivalDirection;
  trainComData.departureDirection = &departureDirection;
  
  //switch control data initialization
  switchControlData.nLock = &nLock;
  switchControlData.eLock = &eLock;
  switchControlData.wLock = &wLock;
  switchControlData.sLock = &sLock;
  switchControlData.trainPresent = &trainPresent;
  switchControlData.trainSize = &trainSize;
  switchControlData.traversalTime = &traversalTime;
  switchControlData.gridlock = &gridlock;
  
  //oled display data initialization
  oledDisplayData.nLock = &nLock;
  oledDisplayData.eLock = &eLock;
  oledDisplayData.wLock = &wLock;
  oledDisplayData.sLock = &sLock;
  oledDisplayData.trainPresent = &trainPresent;
  oledDisplayData.trainArriving = &trainArriving;
  oledDisplayData.trainSize = &trainSize;
  oledDisplayData.traversalTime = &traversalTime;
  oledDisplayData.departureDirection = &departureDirection;
  oledDisplayData.arrivalDirection = &arrivalDirection;
  
  //local keypad data initialization
  localKeypadData.mode = &mode;
  localKeypadData.statusSelection = &statusSelection;
  localKeypadData.scroll = &scroll;
  localKeypadData.select = &select;
  localKeypadData.annunciation = &annunciation;
  
  //serial communications data initialization
  serialComData.nLock = &nLock;
  serialComData.eLock = &eLock;
  serialComData.wLock = &wLock;
  serialComData.sLock = &sLock;
  serialComData.gridlock = &gridlock;
  serialComData.trainPresent = &trainPresent;
  serialComData.trainArriving = &trainArriving;
  serialComData.trainSize = &trainSize;
  serialComData.traversalTime = &traversalTime;
  serialComData.departureDirection = &departureDirection;
  serialComData.arrivalDirection = &arrivalDirection;
  
  /*
  arrivingTrain.i = a;
  departingTrain.i = b;
  switchControl.i = c;
  oledDisplay.i = d;
  localKeypad.i = e;
  serialCom.i = f;  
  trainCom.i = g;*/
  
}

// Purpose: initialize the Task Control Blocks (TCB) for the tasks
void initializeTaskControlBlocks()
{
  void* convertToVoidPtr;
  
  //arriving train task control block initialization
  convertToVoidPtr = &arrivingTrainData;
  arrivingTrain.taskDataPtr = convertToVoidPtr;
  arrivingTrain.myTask = arrivingTrainFcn;

  //departing train task control block initialization
  convertToVoidPtr = &departingTrainData;
  departingTrain.taskDataPtr = convertToVoidPtr;
  departingTrain.myTask = departingTrainFcn;
  
  //switch control task control block initialization  
  convertToVoidPtr = &switchControlData;
  switchControl.taskDataPtr =  convertToVoidPtr;
  switchControl.myTask = switchControlFcn;  
  
  //train communication task control block initialization
  convertToVoidPtr = &trainComData;
  trainCom.taskDataPtr = convertToVoidPtr;
  trainCom.myTask = trainComFcn;
  
  //oled display task control block initialization
  convertToVoidPtr = &oledDisplayData;  
  oledDisplay.taskDataPtr = convertToVoidPtr;
  oledDisplay.myTask = oledDisplayFcn;
   
  //local keypad task control block initialization
  convertToVoidPtr = &localKeypadData;  
  localKeypad.taskDataPtr = convertToVoidPtr;
  localKeypad.myTask = localKeypadFcn;
  
  //serial communciations task control block initialization
  convertToVoidPtr = &serialComData;  
  serialCom.taskDataPtr = convertToVoidPtr;
  serialCom.myTask = serialComFcn;
}

// Purpose: Initialize the display, GPIO and PWN on the board
void initializeBoard() 
{   
  
  // Set the clocking to run directly from the crystal.
  SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
                   SYSCTL_XTAL_8MHZ);
  
  //########Initialize Hardware interrupts#########################
  
  // Enable interrupts to the processor
  IntMasterEnable();
  
  //Enable the GPIO port F and E periferals
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

  //setup trainArrived ISR on sw2 
  GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_1);
  GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
  GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_FALLING_EDGE);
  GPIOPinIntClear(GPIO_PORTF_BASE, GPIO_PIN_1);
  GPIOPinIntEnable(GPIO_PORTF_BASE, GPIO_PIN_1);
  IntEnable(INT_GPIOF);
  
  //setup interupts on sw3-6 for keypad presses
  //sw3 P0= scroll up
  //sw4 P1= scroll down
  //sw5 P2= mode toggle
  //sw6 P3= select train in Arrival queue
  GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
  GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
  GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_FALLING_EDGE);
  GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
  GPIOPinIntEnable(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
  IntEnable(INT_GPIOE);
 
  //########Initialize GPIO port for flashing
  // Enable the GPIO port that is used for the on-board LED
  //SYSCTL_RCGC2_R = SYSCTL_RCGC2_GPIOF;
  
  // Enable the GPIO pin for the LED (PF0).  Set the direction as output, and
  // enable the GPIO pin for digital function.
  // GPIO_PORTF_DIR_R = 0x01;
  // GPIO_PORTF_DEN_R = 0x01;
  
  //########Initialize PWM for Beeping##############
  unsigned long ulPeriod; 
  SysCtlPWMClockSet(SYSCTL_PWMDIV_1);
  
  // Enable the PWM and GPIO used to control it
  SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
  
  // Set GPIO C0 and G1 as PWM pins in order to output the PWM0 and
  // PWM1 signals.
  GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_3);
  GPIOPinTypePWM(GPIO_PORTG_BASE, GPIO_PIN_1);
  
  // Compute the PWM period based on the system clock.
  ulPeriod = SysCtlClockGet() / 440;

  // Set the PWM period to 440 (A) Hz.
  PWMGenConfigure(PWM_BASE, PWM_GEN_0,
                  PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
  PWMGenPeriodSet(PWM_BASE, PWM_GEN_0, ulPeriod);

  // Set PWM0 to a duty cycle of 25% and PWM1 to a duty cycle of 75%.
  PWMPulseWidthSet(PWM_BASE, PWM_OUT_0, ulPeriod / 4);
  PWMPulseWidthSet(PWM_BASE, PWM_OUT_1, ulPeriod * 3 / 4);

  // Enable the PWM0 and PWM1 output signals.
  PWMOutputState(PWM_BASE, PWM_OUT_0_BIT | PWM_OUT_1_BIT, true);

  //#########Initialize the OLED display#################
  RIT128x96x4Init(1000000);
  
  //#########Initialize timer################
  
  // Enable the timer peripheral
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

  // Enable processor interrupts.
  IntMasterEnable();

  // Configure the 32-bit periodic timer for 10th of a second ticks
  TimerConfigure(TIMER0_BASE, TIMER_CFG_32_BIT_PER);
  TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet()/10);

  // Setup the interrupts for the timer timeouts.
  IntEnable(INT_TIMER0A);
  TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

  // Enable the timer.
  TimerEnable(TIMER0_BASE, TIMER_A);

  //###########initialize the UART###################################
  
  //Enable the UART and GPIO periferals
  SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  
  // Enable Processor interrupts
  IntMasterEnable();
  
  // Set GPIO A0 and A1 as UART pins.
  GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
  
  //Configure the UART for 115,200, 8-N-1 operation.
  UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
                      (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                       UART_CONFIG_PAR_NONE));
  //enable UART interrupts
  IntEnable(INT_UART0);
  //Enable recieve and transmit interrupts
  UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
  
  
}


//Purpose:       disconnects the remTCB from a doubly linked list 
//Preconditions: 1) the TCB to be removed must be guaranteed to be in the list
//               this function will not return if it is not
//               2) the linked list is a loop with head TCB connected to tail TCB
//                  and tail TCB connected to head TCB
//               3) head is not NULL and remTCB is not NULL
//postconditions: the item remTCB is removed from the list and the list remains a loop
void deleteTCB(struct TaskControlBlock* head, struct TaskControlBlock* remTCB)
{
   TCB* TCBptr = head; //point to the front of the linked list
   
   //while the search pointer is not pointing at the address of the object to be removed
   //advance the search pointer
   while(TCBptr != remTCB) 
      TCBptr = TCBptr->next;
  
   //now that the search pointer is pointing to the item to be removed
   TCBptr->prev->next = TCBptr->next; //set the previous items next pointer to the item infront of 
                                  //the item being removed X-><-Y-><-Z goes to X->Z  
   TCBptr->next->prev = TCBptr->prev; //set the next items previous pointer to the item behind the
                                  //item being removed  X ->Y->Z goes to X<-Z
   //now we have X-><-Z
}

//Purpose:       Connects a TCB to a doubly linked list after  insertion point and
//               maintains the loop configuration
//Preconditions: 1) The linked list is a loop with head TCB connected to tail TCB
//                  and tail TCB connected to head TCB
//               2) insertionPoint is not NULL and remTCB is not NULL
//postconditions: the item remTCB is removed from the list and the list remains a loop
void insert(struct TaskControlBlock* insertionPoint, struct TaskControlBlock* insertedTCB)
{
   //connect insertedTCB backwards to the TCB at the insertion point, and forwards to the 
   //TCB after the insertion point. 
   insertedTCB->prev = insertionPoint;   
   insertedTCB->next = insertionPoint->next; 
   
   //change forward connection of the TCB at the insertion point to the insertedTCB
   insertionPoint->next = insertedTCB;
}

int main()
{
    //initialization step
    initializeVariables();
    initializeTaskData();
    initializeBoard();
    
    //setup doubly linked list for task queue
    arrivingTrain.prev = &trainCom;
    arrivingTrain.next = &departingTrain;
    departingTrain.prev = &arrivingTrain;
    departingTrain.next = &switchControl;
    switchControl.prev = &departingTrain;
    switchControl.next = &oledDisplay;
    oledDisplay.prev = &switchControl;
    oledDisplay.next = &localKeypad;
    localKeypad.prev = &oledDisplay;
    localKeypad.next = &serialCom;
    serialCom.prev = &localKeypad;
    serialCom.next = &trainCom;
    trainCom.prev = &serialCom;
    trainCom.next = &arrivingTrain;
   
    TCB* currentTCB = &arrivingTrain;
    Bool tcDeleted = FALSE;
    Bool tcAdded = FALSE;
    Bool scDeleted = FALSE;
    Bool scAdded = FALSE;
    
    while(1)
    {     
       //if trainCom hasn't already been added to the queue and checkTrain is true
       //add it to the queue. 
       if((checkTrain) && (!tcAdded))
       {  
          tcAdded = TRUE;
          tcDeleted = FALSE;
          insert(&localKeypad,&trainCom);
       }
       
       //if trainCom hasn't already been removed from the queue and checkTrain is false
       //remove it from the queue
       if(!checkTrain && (!tcDeleted))
       {
          tcDeleted = TRUE;
          tcAdded = FALSE;
          deleteTCB(&arrivingTrain,&trainCom);
       }
       
       //if serialCom hasn't already been added to the queue and no warnings or arrriving trains
       //add it to the queue. 
       if(((trainArriving > 0)||gridlock) && (!scAdded))
       {  
          scAdded = TRUE;
          scDeleted = FALSE;
          insert(&localKeypad,&serialCom);
       }
       
       //if serialCom hasn't already been removed from the queue and no warnings or arrriving trains
       //remove it from the queue
       if(((trainArriving == 0)&&(!gridlock)) && (!scDeleted))
       {
          scDeleted = TRUE;
          scAdded = FALSE;
          deleteTCB(&arrivingTrain,&serialCom);
       }

       //call the task for the givin TCB
       //tried to do (currentTCB->myTask)((void*)(currentTCB->taskDataPtr)));
       //but couldn't dereference any of the pointers to get tastFcns or pointers
       //to task data structs. 
       //which didn't make sense.
       if(currentTCB == &arrivingTrain)
         arrivingTrainFcn(arrivingTrain.taskDataPtr);
       else if(currentTCB == &departingTrain)
         departingTrainFcn(departingTrain.taskDataPtr);
       else if(currentTCB == &switchControl)
         switchControlFcn(switchControl.taskDataPtr);
       else if(currentTCB == &oledDisplay)
         oledDisplayFcn(oledDisplay.taskDataPtr);
       else if(currentTCB == &localKeypad)
         localKeypadFcn(localKeypad.taskDataPtr);
       if(currentTCB == &serialCom)
          serialComFcn(serialCom.taskDataPtr);
       else if(currentTCB == &trainCom)
         trainComFcn(trainCom.taskDataPtr);
         
       //advance to the next task in the queue
       currentTCB = currentTCB->next;      
    }

  return 0;
}
