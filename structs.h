/* 
  Alexandra Gaspar, Evan Poncelet
  October 25, 2013

  Description: This header file defines the global variables needed and all the
               structs to be used in the rest of the program
*/

#ifndef STRUCTS_H_
#define STRUCTS_H_

//simulate boolean type
enum myBool { FALSE = 0, TRUE = 1 };
typedef enum myBool Bool;

//Global variables
extern unsigned short arrivalDirection;
extern unsigned short departureDirection;
extern unsigned short trainSize;
extern unsigned short traversalTime;
extern unsigned short trainArriving; 
extern unsigned short arrivingWhistle;
extern unsigned short departingWhistle;
extern unsigned short mode;
extern unsigned short statusSelection;
extern unsigned short scroll;
extern unsigned short select;
extern unsigned short annunciation;
extern Bool checkTrain;
extern Bool trainPresent;
extern Bool gridlock;
extern Bool nLock;
extern Bool eLock;
extern Bool wLock;
extern Bool sLock;
extern unsigned int counter;

//Additional Globals
extern unsigned short fiveSecondCount;
extern unsigned short fourSecondCount;
extern unsigned short threeSecondCount;
extern unsigned short twoSecondCount;
extern unsigned short oneSecondCount;

extern unsigned int arrivalTime;
extern Bool annuncArrival;
extern Bool annuncDeparture;

//parameters for Arrival blasts
extern int aBeepStartCount;
extern Bool aBeepLongMode;
extern Bool aBeepSilenceMode;
extern int aBeepLONG_BLAST;
extern int aBeepSHORT_BLAST;
extern int aBeepSILENCE;
extern int aBeepTOTAL_LONG_BLASTS;
extern int aBeepTOTAL_SHORT_BLASTS;
extern int aBeepNumLongBlasts;
extern int aBeepNumShortBlasts;

//parameters for Departure blasts
extern int dBeepStartCount;
extern Bool dBeepLongMode;
extern Bool dBeepSilenceMode;
extern int dBeepLONG_BLAST;
extern int dBeepSHORT_BLAST;
extern int dBeepSILENCE;
extern int dBeepTOTAL_LONG_BLASTS;
extern int dBeepTOTAL_SHORT_BLASTS;
extern int dBeepNumLongBlasts;
extern int dBeepNumShortBlasts;


//Task control block
struct TaskControlBlock
{
  void (*myTask)(void*);
  void* taskDataPtr;
  char* i;
  struct TaskControlBlock* next;
  struct TaskControlBlock* prev;
};
typedef struct TaskControlBlock TCB;


//Arriving train data structs
struct ArrivingTrainStruct
{
  unsigned short* trainArriving;
  Bool* checkTrain;
  unsigned short* arrivalDirection;
};
typedef struct ArrivingTrainStruct ArrivingTrainStruct;

//Departing train data structs
struct DepartingTrainStruct
{
  Bool* trainPresent;
  unsigned short* departureDirection;
};
typedef struct DepartingTrainStruct DepartingTrainStruct;


//Train communication data struct
struct TrainComStruct
{
  Bool* trainPresent;
  Bool* checkTrain;
  unsigned short* trainSize;
  unsigned short* trainArriving;
  unsigned short* arrivalDirection;
  unsigned short* departureDirection;
};
typedef struct TrainComStruct TrainComStruct;

//OLED display data struct
struct DisplayStruct
{
  Bool* nLock;
  Bool* eLock;
  Bool* sLock;
  Bool* wLock;
  Bool* trainPresent;
  unsigned short* trainArriving;
  unsigned short* trainSize; 
  unsigned short* traversalTime;
  unsigned short* arrivalDirection;
  unsigned short* departureDirection;
};
typedef struct DisplayStruct DisplayStruct;

//Switch control data struct
struct SwitchControlStruct
{
  Bool* nLock;
  Bool* eLock;
  Bool* sLock;
  Bool* wLock;
  Bool* trainPresent;
  Bool* gridlock;
  unsigned short* trainSize; 
  unsigned short* traversalTime;
};
typedef struct SwitchControlStruct SwitchControlStruct;

//local keypad struct
struct LocalKeypadStruct
{
  unsigned short* mode;
  unsigned short* statusSelection;
  unsigned short* scroll;
  unsigned short* select;
  unsigned short* annunciation; 
};
typedef struct LocalKeypadStruct LocalKeypadStruct;

//serial communications data
struct SerialComStruct
{
  Bool* nLock;
  Bool* eLock;
  Bool* wLock;
  Bool* sLock;
  Bool* gridlock;
  Bool* trainPresent;
  unsigned short* trainArriving;
  unsigned short* trainSize;
  unsigned short* traversalTime;
  unsigned short* arrivalDirection;
  unsigned short* departureDirection;
};
typedef struct SerialComStruct SerialComStruct;
#endif